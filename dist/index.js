"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const app = (0, express_1.default)();
const porta = 3000;
app.get("/", function (req, res) {
    res.send(`That's all Folks!!`);
});
app.listen(porta, function () {
    console.log(`Servidor rodando normalmente na porta ${porta}`);
    console.log(`Acesse http://localhost:${porta}`);
});
// function nome(nome: string): string{
//     return nome.toUpperCase();
// }
